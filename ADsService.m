//
//  ADsService.m
//  VideoTubeAppota
//
//  Created by Huong Nguyen on 3/28/14.
//  Copyright (c) 2014 truonghoang. All rights reserved.
//

#import "ADsService.h"

static ADsService *advise1;
static ADsService *advise2;

NSString *ADsServiceCountriesSupportIADsCodes = @"AU,CA,FR,DE,IE,IT,HK,JP,MX,NZ,ES,TW,US,GB";

@interface ADsService (){
    GADBannerView *admobBannerView;
    GADInterstitial *popUpView;
    ADBannerView *iADsBannerView;
    NSInteger numberFailedRequestIADs;
    CGRect colapViewOldFrame;
    NSInteger numberOfFailed;
    SupportIADsBannerType supportBannerType;
}

@end

@implementation ADsService

@synthesize allowAutoChangeBannerType,currentBannerType,showingStatus,bannerView,colapView,viewShowBanner,hiddenBannerType,adMobBannerRootViewControlller,startBannerPoint;

#pragma mark - commont functions
+ (ADsService *)advise1{
    if (!advise1) {
        advise1 = [[ADsService alloc] init];
    }
    return advise1;
}

+ (ADsService *)advise2{
    if (!advise2) {
        advise2 = [[ADsService alloc] init];
    }
    return advise2;
}

#pragma mark - public functions
- (void)showForView:(UIView *)newViewShowBanner
 atStartBannerPoint:(CGPoint)newStartBannerPoint
      withColapView:(UIView *)newColapView
   andBannerRootVCT:(UIViewController *)newBannerRootVCT{
    self.viewShowBanner = newViewShowBanner;
    self.colapView = newColapView;
    self.adMobBannerRootViewControlller = newBannerRootVCT;
    self.startBannerPoint = newStartBannerPoint;
    supportBannerType = SupportIADsBannerTypeUnknow;
    [self startService];
}

- (void)stopService{
    showingStatus = ShowingStatusHidden;
    [self hiddenView];
    iADsBannerView = nil;
    admobBannerView = nil;
    bannerView = nil;
}

- (void)startService{
    numberOfFailed = 0;
    if (showingStatus != ShowingStatusHidden) {
        return;
    }

    if (currentBannerType == BannerTypeAdMob) {
        showingStatus = ShowingStatusWaitingShow;
        [self.admobBannerView loadRequest:[self createGADRequest]];
    }else{
        showingStatus = ShowingStatusWaitingShow;
        [self bannerView];
    }
}

/* for pop up */
- (void)getAndShowPopUp{
    [self.popUpView loadRequest:[self createGADRequest]];
}

- (void)stopGetPopUpIfIGetting{
    popUpView = nil;
}

#pragma mark - private functions
- (id)init{
    self = [super init];
    if (self) {
        [self configDefault];
    }
    return self;
}

- (void)configDefault{
    numberFailedRequestIADs = 0;
    allowAutoChangeBannerType = YES;
    if (![self setCurrentBannerType:BannerTypeIADs]) {
        [self setCurrentBannerType:BannerTypeAdMob];
    }
    hiddenBannerType = HiddenBannerTypeNoAction;
    showingStatus = ShowingStatusHidden;
}

- (GADRequest *)createGADRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
    request.testDevices =
    [NSArray arrayWithObjects:
     // TODO: Add your device/simulator test identifiers here. They are
     // printed to the console when the app is launched.
     nil];
    return request;
}

- (void)hiddenView{
    [viewShowBanner removeFromSuperview];
    colapView.frame = colapViewOldFrame;
}

- (void)displayView{
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect bannerViewFrame = self.bannerView.frame;
        NSLog(@"start colap y %f old %f banner y %f %f %f",colapView.frame.origin.y,colapViewOldFrame.origin.y,self.bannerView.frame.size.height,colapView.center.y, colapViewOldFrame.origin.y+colapViewOldFrame.size.height/2+self.bannerView.frame.size.height);
        switch (hiddenBannerType) {
            case HiddenBannerTypeChangeFrameToTop:{
                colapView.center = CGPointMake(colapView.center.x, colapViewOldFrame.origin.y+colapViewOldFrame.size.height/2+self.bannerView.frame.size.height);
                bannerViewFrame.origin.y = colapViewOldFrame.origin.y;
            }
                break;
            case HiddenBannerTypeChangeFrameToBottom:{
                colapView.center = CGPointMake(colapView.center.x, colapViewOldFrame.origin.y+colapViewOldFrame.size.height/2-self.bannerView.frame.size.height);
                bannerViewFrame.origin.y = colapViewOldFrame.origin.y + colapViewOldFrame.size.height - bannerViewFrame.size.height;
            }
                break;
            case HiddenBannerTypeResizeFrameToBottom:{
                CGRect frame = colapViewOldFrame;
                frame.size.height -= self.bannerView.frame.size.height;
                colapView.frame = frame;
                bannerViewFrame.origin.y = colapViewOldFrame.origin.y + colapViewOldFrame.size.height - bannerViewFrame.size.height;
            }
                break;
            case HiddenBannerTypeResizeFrameToTop:{
                CGRect frame = colapViewOldFrame;
                frame.size.height -= self.bannerView.frame.size.height;
                frame.origin.y += self.bannerView.frame.size.height;
                colapView.frame = frame;
                bannerViewFrame.origin.y = colapViewOldFrame.origin.y;
            }
                break;
            default:{
                bannerViewFrame.origin = self.startBannerPoint;
            }
                break;
        }
        
        self.bannerView.frame = bannerViewFrame;
        [viewShowBanner addSubview:self.bannerView];
        NSLog(@"end colap y %f old %f banner y %f",colapView.frame.origin.y,colapViewOldFrame.origin.y,self.bannerView.frame.origin.y);
    });
}

- (SupportIADsBannerType)caculateSupportIADsBannerType{
    [[LocationUtility shareInstance] updateCurrentPlaceMark:^(CLPlacemark *currentPlaceMark) {
        NSLog(@"did get place mark %@",currentPlaceMark);
        if (!currentPlaceMark||[ADsServiceCountriesSupportIADsCodes rangeOfString:currentPlaceMark.ISOcountryCode].location == NSNotFound) {
            supportBannerType = SupportIADsBannerTypeNo;
        }else{
            supportBannerType = SupportIADsBannerTypeYes;
        }
    }];
    return supportBannerType;
}
#pragma mark - delegates, datasources

#pragma mark Ad Request Lifecycle Notifications
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    if (showingStatus == ShowingStatusWaitingShow) {
        [self displayView];
    }
    showingStatus = ShowingStatusADsDisplay;
    NSLog(@"adViewDidReceiveAd");
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    showingStatus = ShowingStatusADsNoDisplay;
    NSLog(@"didFailToReceiveAdWithError");
    ++numberOfFailed;
    if (numberOfFailed >= kMaxGetADsFail) {
        [self setCurrentBannerType:BannerTypeAdMob];
    }
}

#pragma mark Click-Time Lifecycle Notifications
- (void)adViewWillPresentScreen:(GADBannerView *)adView{
    NSLog(@"adViewWillPresentScreen");
}

- (void)adViewWillDismissScreen:(GADBannerView *)adView{
    NSLog(@"adViewWillDismissScreen");
}

- (void)adViewDidDismissScreen:(GADBannerView *)adView{
    NSLog(@"adViewDidDismissScreen");
}

- (void)adViewWillLeaveApplication:(GADBannerView *)adView{
    NSLog(@"adViewWillLeaveApplication");
}


#pragma mark Ad Request Lifecycle Notifications
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    NSLog(@"- (void)interstitialDidReceiveAd:(GADInterstitial *)ad");
    [ad presentFromRootViewController:self.adMobBannerRootViewControlller];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error");
}

#pragma mark Display-Time Lifecycle Notifications
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad{
    NSLog(@"- (void)interstitialWillPresentScreen:(GADInterstitial *)ad");
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad{
    NSLog(@"- (void)interstitialWillDismissScreen:(GADInterstitial *)ad");
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad{
    NSLog(@"- (void)interstitialDidDismissScreen:(GADInterstitial *)ad");
    popUpView = nil;
}



- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad{
    NSLog(@"- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad");
}

#pragma mark - ADBannerDelegate
- (void)bannerViewWillLoadAd:(ADBannerView *)banner __OSX_AVAILABLE_STARTING(__MAC_NA, __IPHONE_5_0){
        NSLog(@"- (void)bannerViewWillLoadAd:(ADBannerView *)banner __OSX_AVAILABLE_STARTING(__MAC_NA, __IPHONE_5_0)");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
        NSLog(@"- (void)bannerViewDidLoadAd:(ADBannerView *)banner");
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
        NSLog(@"- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error %@",error);
    if (error.code == ADErrorUnknown||
        error.code == ADErrorServerFailure||
        error.code == ADErrorLoadingThrottled||
        error.code == ADErrorBannerVisibleWithoutContent||
        error.code == ADErrorAdUnloaded) {
        ++numberOfFailed;
    }else{
        numberOfFailed = 100000;
    }
    if (numberOfFailed >= kMaxGetADsFail) {
        [self setCurrentBannerType:BannerTypeAdMob];
    }
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{
        NSLog(@"- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave");
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner{
    NSLog(@"- (void)bannerViewActionDidFinish:(ADBannerView *)banner");
}

#pragma mark - setters, getters
- (GADInterstitial *)popUpView{
    if (!popUpView) {
        popUpView = [[GADInterstitial alloc] init];
        popUpView.delegate = self;
        
        popUpView.adUnitID = kKeyAdMob;
    }
    return popUpView;
}

- (GADBannerView *)admobBannerView{
    if (!admobBannerView) {
        admobBannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:CGPointMake(0, 0)];
        admobBannerView.adUnitID = kKeyAdMob;
        admobBannerView.delegate = self;
        admobBannerView.rootViewController = adMobBannerRootViewControlller;
    }
    return admobBannerView;
}

- (ADBannerView *)iADsBannerView{
    if (!iADsBannerView) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            iADsBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        }else{
            iADsBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeMediumRectangle];
        }
        iADsBannerView.center = CGPointMake(iADsBannerView.frame.size.width/2, iADsBannerView.frame.size.height/2);
        iADsBannerView.delegate = self;
    }
    return iADsBannerView;
}

- (UIView *)bannerView{
    if (!bannerView) {
        bannerView = [[UIView alloc] init];
        UIView *subView = currentBannerType == BannerTypeAdMob ? self.admobBannerView : self.iADsBannerView;
        bannerView.frame = subView.frame;
        [bannerView addSubview:subView];
    }
    return bannerView;
}

- (BOOL)setCurrentBannerType:(BannerType)newCurrentBannerType{
    if (newCurrentBannerType==BannerTypeIADs&&supportBannerType==SupportIADsBannerTypeUnknow) {
        if ([self caculateSupportIADsBannerType] == SupportIADsBannerTypeNo) {
            return NO;  
        }
    }
    
    if (newCurrentBannerType==currentBannerType) {
        return YES;
    }
    currentBannerType = newCurrentBannerType;
    
    if (!bannerView) {
        return YES;
    }
    
    if (!allowAutoChangeBannerType) {
        allowAutoChangeBannerType = YES;
    }
    
    [self stopService];
    [self startService];
    return YES;
}

- (void)setViewShowBanner:(UIView *)newViewShowBanner{
    viewShowBanner = newViewShowBanner;
    if (bannerView) {
        [viewShowBanner addSubview:bannerView];
    }
}

- (void)setAdMobBannerRootViewControlller:(UIViewController *)newBannerRootViewControlller{
    adMobBannerRootViewControlller = newBannerRootViewControlller;
}

- (void)setColapView:(UIView *)newColapView{
    colapView = newColapView;
    colapViewOldFrame = colapView.frame;
    if (bannerView) {
        [self displayView];
    }
}

- (void)setHiddenBannerType:(HiddenBannerType)newHiddenBannerType{
    hiddenBannerType = newHiddenBannerType;
    if (bannerView) {
        [self displayView];
    }
}

@end
