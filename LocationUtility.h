//
//  LocationUtility.h
//  App4Kids
//
//  Created by lithewall on 3/18/14.
//  Copyright (c) 2014 D3Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^SuccessUpdatePlaceMark)(CLPlacemark *currentPlaceMark);
extern NSString *LocationUtilityDidGetNewLocationNotification;

@interface LocationUtility : NSObject<CLLocationManagerDelegate>

+ (NSArray *)getAllLanguageIdentifiers;
+ (LocationUtility *)shareInstance;

//mặc định là kiểm tra theo mét
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocationAccuracy currentLocationAccuracy;
@property (nonatomic, readonly) CLLocation *currentLocation;
@property (nonatomic, readonly) CLPlacemark *currentPlaceMark;
@property (nonatomic, copy) SuccessUpdatePlaceMark successUpdatePlace;
- (CLAuthorizationStatus)startGetLocationAndGetAuthorizationStatus;
- (void)stopGetLocation;
- (void)updateCurrentPlaceMark:(SuccessUpdatePlaceMark) success;
@end
