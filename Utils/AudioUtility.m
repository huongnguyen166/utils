//
//  AudioUtility.m
//  ArtFile
//
//  Created by truonghm on 3/18/14.
//  Copyright (c) 2014 truonghm. All rights reserved.
//

#import "AudioUtility.h"


@implementation AudioUtility

+ (void)artImageFromSoundURL:(NSURL *)fileURL success:(void(^)(UIImage *image))success{
    AVAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
    
    NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                           withKey:AVMetadataCommonKeyArtwork
                                                          keySpace:AVMetadataKeySpaceCommon];
        
        for (AVMetadataItem *item in artworks) {
            if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                NSDictionary *dict = [item.value copyWithZone:nil];
                success([UIImage imageWithData:[dict objectForKey:@"data"]]);
            } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                success([UIImage imageWithData:[item.value copyWithZone:nil]]);
            }
        }

    }];
}

@end
