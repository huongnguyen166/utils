//
//  VideoUtility.h
//  OpenAllVideoFormat
//
//  Created by truonghm on 2/28/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLFile.h"
#import "VLCThumbnailsCache.h"


extern NSString *defaultPlaylistName;

typedef void (^GenImageSuccess)(UIImage *image);

@interface VideoUtility : NSObject{
    NSMutableArray *listURL;
    NSMutableArray *successs;
    GenImageSuccess currentSuccess;
    BOOL isDownloadingImage;
    MLFile *currentFile;
}
+ (VideoUtility *)shareInstance;

- (void)imageForVideoURL:(NSURL *)url success:(GenImageSuccess) success;
- (void)resetDownload;
@end
