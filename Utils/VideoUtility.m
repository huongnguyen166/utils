//
//  VideoUtility.m
//  OpenAllVideoFormat
//
//  Created by truonghm on 2/28/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import "VideoUtility.h"
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>

static VideoUtility *shareInstance;

NSString *defaultPlaylistName = @"defaultPlaylistName";

@implementation VideoUtility

- (id)init{
    self = [super init];
    if (self) {
        listURL = [[NSMutableArray alloc] init];
        successs = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (VideoUtility *)shareInstance{
    if (!shareInstance) {
        shareInstance = [[VideoUtility alloc] init];
    }
    return shareInstance;
}

- (void)imageForVideoURL:(NSURL *)url success:(GenImageSuccess) success{
    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:url];
    UIImage  *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    success(thumbnail);
//    [listURL addObject:url];
//    [successs addObject:success];
//    [self checkAndDownloadImage];
}

- (void)removeFirstURLAndContinue{
    [listURL removeObjectAtIndex:0];
    [successs removeObjectAtIndex:0];
    [self checkAndDownloadImage];
}

- (void)checkAndDownloadImage{
    if (listURL.count!=0&&!isDownloadingImage) {
        isDownloadingImage = YES;

        NSURL *url = [listURL objectAtIndex:0];
        NSArray *filePaths = [NSArray arrayWithObject:url.path];
        [[MLMediaLibrary sharedMediaLibrary] addFilePaths:filePaths];
        currentSuccess = [successs objectAtIndex:0];
        UIImage *image = [self getPreviewImage];
        if (image&&UIImageJPEGRepresentation(image, 1).length>15000) {
            NSLog(@"Success get image data = %d",UIImageJPEGRepresentation(image, 1).length);
            [self postSuccessForCurrentDownloadWithImage:image];
        }
    }
}

- (void)postSuccessForCurrentDownloadWithImage:(UIImage *)image{
    currentSuccess(image);
    isDownloadingImage = NO;
    [self removeFirstURLAndContinue];
}

- (UIImage *)getPreviewImage{
    [[MLMediaLibrary sharedMediaLibrary] updateMediaDatabase];
    NSArray *allFiles = [MLFile allFiles];
    
    NSURL *url = [listURL objectAtIndex:0];
    for (MLFile *file in allFiles) {
        UIImage *imageOfFile = [VLCThumbnailsCache thumbnailForMediaFile:file];
        NSLog(@"url %@ preview %d",file.url,UIImageJPEGRepresentation(imageOfFile, 1).length);
        if ([url.description isEqualToString:file.url]) {
            currentFile = file;
            NSLog(@"did change search new file %@",url);
            break;
        }
    }
    
//    NSString *obseverkey = [NSString stringWithFormat:@"%d",currentFile.hash];
    [currentFile addObserver:self forKeyPath:@"computedThumbnail" options:0 context:nil];
    if ([currentFile respondsToSelector:@selector(willDisplay)]){
        NSLog(@"will display photo preview");
        [currentFile willDisplay];
    }
    return [VLCThumbnailsCache thumbnailForMediaFile:currentFile];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    NSLog(@"keypath = %@",keyPath);
    NSLog(@"Success get image data = %d",UIImageJPEGRepresentation([VLCThumbnailsCache thumbnailForMediaFile:currentFile], 1).length);
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%d",currentFile.hash] object:Nil];
    if ([keyPath isEqualToString:@"computedThumbnail"]) {
        [self postSuccessForCurrentDownloadWithImage:[VLCThumbnailsCache thumbnailForMediaFile:currentFile]];
    }
}

- (void)resetDownload{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    isDownloadingImage = NO;
    [listURL removeAllObjects];
    [successs removeAllObjects];

}
@end
