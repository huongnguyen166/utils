//
//  ADsService.h
//  VideoTubeAppota
//
//  Created by Huong Nguyen on 3/28/14.
//  Copyright (c) 2014 truonghoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GADBannerView.h"
#import "GADInterstitial.h"
#import <iAd/iAd.h>
#import "LocationUtility.h"

typedef enum{
    HiddenBannerTypeResizeFrameToTop,
    HiddenBannerTypeNoAction,
    HiddenBannerTypeChangeFrameToTop,
    HiddenBannerTypeChangeFrameToBottom,
    HiddenBannerTypeResizeFrameToBottom
}HiddenBannerType;

typedef enum {
    BannerTypeIADs,
    BannerTypeAdMob
}BannerType;

typedef enum {
    ShowingStatusHidden,
    ShowingStatusADsDisplay,
    ShowingStatusADsNoDisplay,
    ShowingStatusWaitingShow
}ShowingStatus;

typedef enum {
    SupportIADsBannerTypeUnknow,
    SupportIADsBannerTypeNo,
    SupportIADsBannerTypeYes
}SupportIADsBannerType;

/* for banner */
extern NSString *ADsServiceDidClickBannerNotification;
extern NSString *ADsServiceDidChangeBannerTypeNotification;
extern NSString *ADsServiceDidChangeShowingStatusBannerNotification;
extern NSString *ADsServiceCannotGetAnyADsBannerNotification;
extern NSString *ADsServiceCountriesSupportIADsCodes;
/* for pop up */
extern NSString *ADsServiceCannotGetPopUpNotification;
extern NSString *ADsServiceDidShowPopupNotification;

#pragma mark - configs
#define kAdMobId @"a152613285577a9"
#define kMaxGetADsFail 2

@interface ADsService : NSObject<ADBannerViewDelegate,GADInterstitialDelegate,GADBannerViewDelegate>

/* for banner */
@property (nonatomic) BOOL allowAutoChangeBannerType;
@property (nonatomic, readonly) BannerType currentBannerType;
@property (nonatomic, readonly) ShowingStatus showingStatus;
@property (nonatomic) CGPoint startBannerPoint;
@property (nonatomic, readonly) UIView *bannerView;
@property (nonatomic, copy) UIView *colapView;
@property (nonatomic, copy) UIView *viewShowBanner;
@property (nonatomic) HiddenBannerType hiddenBannerType;
@property (nonatomic) UIViewController *adMobBannerRootViewControlller;

- (void)showForView:(UIView *)viewShowBanner
 atStartBannerPoint:(CGPoint)startBannerPoint
      withColapView:(UIView *)colapView
   andBannerRootVCT:(UIViewController *)bannerRootVCT;
- (void)stopService;
- (void)startService;
- (BOOL)setCurrentBannerType:(BannerType)newCurrentBannerType;

/* for pop up */
@property (nonatomic, readonly) BOOL isShowingPopUp;
- (void)getAndShowPopUp;
- (void)stopGetPopUpIfIGetting;

+ (ADsService *)advise1;
+ (ADsService *)advise2;

@end
