//
//  LocationUtility.m
//  App4Kids
//
//  Created by lithewall on 3/18/14.
//  Copyright (c) 2014 D3Team. All rights reserved.
//

#import "LocationUtility.h"

static LocationUtility *shareInstance;
NSString *LocationUtilityDidGetNewLocationNotification = @"LocationUtilityDidGetNewLocationNotification";

@implementation LocationUtility

@synthesize locationManager, currentLocation, currentPlaceMark,currentLocationAccuracy;

#pragma mark - commont functions
+ (LocationUtility *)shareInstance{
    if (!shareInstance) {
        shareInstance = [[LocationUtility alloc] init];
    }
    
    return shareInstance;
}

+ (NSArray *)getAllLanguageIdentifiers{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
}

#pragma mark - public functions
- (CLAuthorizationStatus)startGetLocationAndGetAuthorizationStatus{
    [self.locationManager startUpdatingLocation];
    return [CLLocationManager authorizationStatus];
}

- (void)stopGetLocation{
    [self.locationManager stopUpdatingLocation];
}

- (void)updateCurrentPlaceMark:(SuccessUpdatePlaceMark) success{
    _successUpdatePlace = success;
    if ([self startGetLocationAndGetAuthorizationStatus]!=ALAuthorizationStatusAuthorized) {
        NSLog(@"maybe need enable location");
        success(nil);
    }
}

#pragma mark - private functions

#pragma mark - Delegates and DataSources

- (void)locationManager:(CLLocationManager *)manager
	 didUpdateLocations:(NSArray *)locations __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
    NSLog(@"%@", NSStringFromSelector(_cmd));
    if(!currentPlaceMark){
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:self.locationManager.location
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           if (error){
                               NSLog(@"Geocode failed with error: %@", error);
                               return;
                           }
                           
                           currentPlaceMark = [placemarks objectAtIndex:0];
                           if(_successUpdatePlace){
                               _successUpdatePlace(currentPlaceMark);
                           }
        }];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:LocationUtilityDidGetNewLocationNotification object:locationManager.location];
}

- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager  __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    NSLog(@"%@", NSStringFromSelector(_cmd));
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_7_0){
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_7_0){
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)locationManager:(CLLocationManager *)manager
rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
              withError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_7_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region");
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          didEnterRegion:(CLRegion *)region");
}

- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          didExitRegion:(CLRegion *)region");
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          didFailWithError:(NSError *)error");
}

- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(CLRegion *)region
              withError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          monitoringDidFailForRegion:(CLRegion *)region");
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_2){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status");
}

- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_TBD,__IPHONE_5_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager          didStartMonitoringForRegion:(CLRegion *)region");
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
    NSLog(@"- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager");
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
    NSLog(@"- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager");
}

- (void)locationManager:(CLLocationManager *)manager
didFinishDeferredUpdatesWithError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
    NSLog(@"- (void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error");
}


#pragma mark - setters getters
- (CLLocationManager *)locationManager{
    if (!locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = currentLocationAccuracy;
    }
    return locationManager;
}

- (void)setCurrentLocationAccuracy:(CLLocationAccuracy )newCurrentLocationAccuracy{
    currentLocationAccuracy = newCurrentLocationAccuracy;
    self.locationManager.desiredAccuracy = currentLocationAccuracy;
}

- (CLLocation *)currentLocation{
    return self.locationManager.location;
}
@end
